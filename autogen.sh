#!/bin/sh
[ -e config.cache ] && rm -f config.cache

libtoolize --automake
intltoolize --force --automake --copy
aclocal
autoconf
autoheader
automake -a
./configure $@
exit

