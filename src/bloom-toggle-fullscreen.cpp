//      bloom_toggle_fullscreen.cpp
//
//      Copyright 2010 Agorabox
//
//      Author(s) : Antonin Fouques <antonin.fouques@agorabox.org>
//
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.

#include "../config.h"

#include <QObject>
#include <signal.h>
#include <gdk/gdk.h>
#include <locale.h>
#include <glib/gi18n.h>

#include "bloom-toggle-fullscreen.h"

#define _(String) gettext (String)

BloomToggleFullscreen::BloomToggleFullscreen (MplPanelClient *panel_client, QWidget *parent) :
    QWidget (parent),
    panel (panel_client)
{
    move (0, 0);
    setMaximumSize (1, 1);
    fullscreen = false;
}

void
BloomToggleFullscreen::launch_toggle ()
{
    process.start ("toggle-fullscreen");
    mpl_panel_client_request_hide (panel);
    fullscreen = !fullscreen;
    if (fullscreen)
        mpl_panel_client_request_tooltip (panel, _("Switch to windowed mode"));
    else
        mpl_panel_client_request_tooltip (panel, _("Switch to fullscreen mode"));
}
